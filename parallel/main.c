#include "ranging.h"

int main(int argc, char* argv[])
{
    DIR *dir = opendir(argv[1]);
    int num_of_files = 0;
    int i = 0;
    pthread_t thread;
    someArgs_t arg;
    if (!dir)
    {
        fprintf(stderr, "Error opening directory\n");
        return 1;
    }
    struct dirent *ent;
    while((ent = readdir(dir)) != NULL)
    {
        if(ent->d_name[0] != '.') {
            num_of_files++;
        }
    }
    request_con *files = (request_con*)malloc(num_of_files * sizeof(request_con));
    if (files == NULL) {
        return 1;
    }
    rewinddir(dir);
    while((ent = readdir(dir)) != NULL)  //Заполнение files
    {
        if(ent->d_name[0] != '.') {
            sprintf(files[i].name,"%s/%s", argv[1], ent->d_name);
            files[i].f = fopen(files[i].name, "r");
            if(files[i].f == NULL) {
                fprintf(stderr, "Error opening file\n");
                return 1;
            }
            files[i].number_of_rep = 0;
            i++;
        }
    }
    char request[16][256];
    char c = 'a';
    i = 0;
    int j = 0;
    printf("Введите запрос: ");
    while (c != '\n') {
        while ((c = getchar()) != ' ' && c != '\n') {
            request[j][i] = c;
            i++;
        }
        request[j][i + 1] = '\0';
        i = 0;
        j++;
    }
    int tr = 0;
    for(i = 0; i < j; i++) { 
        tr++;
        if (tr == 2) {
            tr = 0;
        }
        if (tr == 1) {
        //search(files, request[i], num_of_files);
        arg.files = files;
        arg.request = request[i];
        arg.num = num_of_files;
        pthread_create(&thread, NULL, search1, (void*) &arg);
        } else {
            search2(files, request[i], num_of_files);
        }
        pthread_join(thread, NULL);
    }
    Sort(files, num_of_files);
    printf("=========ТОП 5 ПОВТОРЕНИЙ=========\n");
    for(i = 0; i < 5; i++) {
        printf("Количество повторений в %s = %d\n", files[i].name, files[i].number_of_rep);
        fclose(files[i].f);
    }
    closedir(dir);
    free(files);
    return 0;
}

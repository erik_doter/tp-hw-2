#ifndef TP_HW_2_RANGING_H_
#define TP_HW_2_RANGING_H_

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>


typedef struct request_con {
	FILE *f;
	int number_of_rep;
	char name[256];
} request_con;

typedef struct someArgs_tag {
    request_con *files;
    char *request;
    int num;
} someArgs_t;

void search2(request_con* files, char* request, int num);
void* search1(void* args);
void Sort(request_con* arr, int n);


#endif /* TP_HW_2_RANGING_H_ */

